
// Test Framework Libraries
#include <catch2/catch.hpp>

// Standared Libraries
#include <iostream>

// External Libraries
#include <glm/glm.hpp>

// Internal Libraries
#include "RREF.hpp"

template <int ROW, int COLUMN>
constexpr std::string printMatrix(const glm::mat<ROW, COLUMN, float> &matrix) {
  std::string matrixString{};
  for (int row{0}; row < ROW; ++row) {
    matrixString.append("\t");
    for (int column{0}; column < COLUMN; ++column) {
        matrixString.append(std::to_string(matrix[row][column]));
        matrixString.erase(matrixString.find_last_not_of('0') + 1, std::string::npos);
        matrixString.erase (matrixString.find_last_not_of('.') + 1, std::string::npos);
        if (column != (COLUMN - 1))
          matrixString.append(", ");
    }
    matrixString.append("\n");
  }
  return matrixString;
}

template <int ROW, int COLUMN>
constexpr std::string printTestCase(const glm::mat<ROW, COLUMN, float> &testCase, const glm::mat<ROW, COLUMN, float> &expectedResult, const glm::mat<ROW, COLUMN, float> &actualResult) {
  std::string matrixString{std::string(5, '-') + " Test Case Input " + std::string(5, '-') + "\n"};
  matrixString.append(printMatrix(testCase));
  matrixString.append(std::string(27, '-') + "\n");
  matrixString.append(std::string(5, '-') + " Expected Result " + std::string(5, '-') + "\n");
  matrixString.append(printMatrix(expectedResult));
  matrixString.append(std::string(27, '-') + "\n");
  matrixString.append(std::string(5, '-') + " Actual Result " + std::string(7, '-') + "\n");
  matrixString.append(printMatrix(actualResult));
  matrixString.append(std::string(27, '-') + "\n");
  return matrixString;
};

TEST_CASE("RREF mat3", "[Math]") {
	SECTION("Identity Matrix") {
		glm::mat3 inputMat3{1.0f};
		REQUIRE(BP::Math::RREF(inputMat3) == inputMat3);
	}

	SECTION("General Input") {
		glm::mat3 inputMat3{{0.0f, 1.0f, -1.0f}, {0.0f, -1.0f, -1.0f}, {0.0f, 0.0f, 0.0f}};
		glm::mat3 expectedOutput{{0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}};
		glm::mat3 actualOutput{BP::Math::RREF(inputMat3)};
    INFO(printTestCase(inputMat3, actualOutput, expectedOutput));
		REQUIRE(actualOutput == expectedOutput);
	}

	SECTION("General Input2") {
		glm::mat3 inputMat3{{1.0f, -1.0f, 1.0f}, {1.0f, 5.0f, 7.0f}, {1.0f, -7.0f, -5.0f}};
		glm::mat3 expectedOutput{{1.0f, 0.0f, 2.0f}, {0.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 0.0f}};
		glm::mat3 actualOutput{BP::Math::RREF(inputMat3)};
    INFO(printTestCase(inputMat3, actualOutput, expectedOutput));
		REQUIRE(actualOutput == expectedOutput);
	}

	SECTION("Empty Column") {
		glm::mat3 inputMat3{{0.0f, -1.0f, 1.0f}, {0.0f, 5.0f, 7.0f}, {0.0f, -7.0f, -5.0f}};
		glm::mat3 expectedOutput{{0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}};
		glm::mat3 actualOutput{BP::Math::RREF(inputMat3)};
    INFO(printTestCase(inputMat3, actualOutput, expectedOutput));
		REQUIRE(actualOutput == expectedOutput);
	}
}

TEST_CASE("RREF mat4", "[Math]") {
	SECTION("Identity Matrix") {
		glm::mat4 inputMat4{1.0f};
		REQUIRE(BP::Math::RREF(inputMat4) == inputMat4);
	}

	SECTION("Change correct Pivot") {
		glm::mat4 inputMat4{{1.0f, 0.0f, 1.0f, 1.0f}, {0.0f, 1.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f, 1.0f}};
		glm::mat4 expectedOutput{{1.0f, 0.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
		glm::mat4 actualOutput{BP::Math::RREF(inputMat4)};
    INFO(printTestCase(inputMat4, actualOutput, expectedOutput));
		REQUIRE(actualOutput == expectedOutput);
	}
}

TEST_CASE("RREF mat3x4", "[Math]") {
	SECTION("Identity Matrix") {
		glm::mat3x4 inputMat3x4{1.0f};
		REQUIRE(BP::Math::RREF(inputMat3x4) == inputMat3x4);
	}

	SECTION("General Input") {
		glm::mat3x4 inputMat3x4{{1.0f, 1.0f, -1.0f, 7.0f}, {1.0f, -1.0f, 2.0f, 3.0f}, {2.0f, 1.0f, 1.0f, 9.0f}};
		glm::mat3x4 expectedOutput{{1.0f, 0.0f, 0.0f, 6.0f}, {0.0f, 1.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 1.0f, -2.0f}};
		glm::mat3x4 actualOutput{BP::Math::RREF(inputMat3x4)};
    INFO(printTestCase(inputMat3x4, actualOutput, expectedOutput));
		REQUIRE(actualOutput == expectedOutput);
	}

	SECTION("First Row Zeros") {
		glm::mat3x4 inputMat3x4{{0.0f, 1.0f, -1.0f, -0.428571403f}, {0.0f, -1.0f, 1.0f, 1.28571427f}, {0.0f, 0.0f, 0.0f, 1.28571427f}};
		glm::mat3x4 expectedOutput{{0.0f, 1.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
		glm::mat3x4 actualOutput{BP::Math::RREF(inputMat3x4)};
    INFO(printTestCase(inputMat3x4, actualOutput, expectedOutput));
		REQUIRE(actualOutput == expectedOutput);
	}
}