#ifndef RREF_H
#define RREF_H

// Standard Libraries
#include <math.h>

// External Libraries
#include <glm/glm.hpp>

namespace BP::Math {
  /// @brief preforms row reduction for the given matrix
  /// @tparam COLUMN number of columns
  /// @tparam ROW number of rows
  /// @param m matrix data
  template <int ROW, int COLUMN>
  static glm::mat<ROW, COLUMN, float> RREF(const glm::mat<ROW, COLUMN, float> &m) {
    glm::mat<ROW, COLUMN, float> matrix{m};
    int pivotRow{0}; ///< Location of the pivot currently working on row, column

    for (int currentColumn{0}; (currentColumn < COLUMN) && (pivotRow < ROW); ++currentColumn) {
      if (iszero(matrix[pivotRow][currentColumn])) {
        bool hasSwapped{false};
        for (int rowUnderPivot{pivotRow + 1}; rowUnderPivot < ROW; ++rowUnderPivot) {
          if (iszero(matrix[rowUnderPivot][currentColumn]))
            continue;
          // swap rows
          glm::vec<COLUMN, float> tempSwap{matrix[pivotRow]};
          matrix[pivotRow] = matrix[rowUnderPivot];
          matrix[rowUnderPivot] = tempSwap;
          hasSwapped = true;
        }

        if (!hasSwapped)
          continue; // Go to next column keep pivot row the same
      }

      // Divide pivot row
      matrix[pivotRow] /= matrix[pivotRow][currentColumn];

      // Reduce other rows to 0
      for (int currentRow{0}; currentRow < ROW; ++currentRow) {
        if (currentRow == pivotRow)
          continue;

        const float zeroOffset{matrix[currentRow][currentColumn]};
        matrix[currentRow] -= (matrix[pivotRow] * zeroOffset);
      }

      ++pivotRow;
    } // End Column for loop
    return matrix;
  } // End RREF function
}

#endif // RREF_H